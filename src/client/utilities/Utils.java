/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

/* Define package structure and import required classes from this project */
package client.utilities;
import client.peer.PeerListObject;

/* Import other useful classes */
import java.util.Random;
import java.net.ServerSocket;
import java.util.ArrayList;

/* Simple class with basic utilities used throughout the project */
public class Utils
{   
    /* Method for converting a byte array into hex values. Returns a string */
	public static String bytesToHex(byte[] in)
    {
        final StringBuilder builder = new StringBuilder();

        for(byte b : in)
        {
            builder.append(String.format("%%%02x", b));
        }

        return builder.toString();
    }

    /* Method for printing out debug messages that indicate what messages the peer is receiving */
    public static void peerDebugMessage(String msg, byte[] peerID, String peerIP, int peerPort)
    {
    	System.out.println("DEBUG: " + msg + " from peer [ID: " + new String(peerID) + " / IP: " + peerIP + ":" + peerPort + "]");
    }

    public static ServerSocket openListener()
    {
        ServerSocket newListener;

        for(int port = 6881; port <= 6889; port++)
        {
            try
            {
                newListener = new ServerSocket(port);
                return newListener;
            }
            catch(Exception e)
            {
                //ERROR
            }
        }

        return null;
    }

    /* Generate a random alphanumeric peer ID */
    public static byte[] generatePeerID()
    {
        Random rnd = new Random(System.currentTimeMillis());
        String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuilder peer_id = new StringBuilder(20);

        for(int i = 0; i < 20; i++)
        {
            peer_id.append(alphabet.charAt(rnd.nextInt(alphabet.length())));
        }

        return (peer_id.toString().getBytes());
    }

    /* Generate a peer list based on known values if we fail to get it from the tracker */
    public static ArrayList<PeerListObject> genFakePeerList()
    {
        PeerListObject peer1 = new PeerListObject();
        PeerListObject peer2 = new PeerListObject();
        ArrayList<PeerListObject> peerList = new ArrayList<PeerListObject>(2);

        peer1.setPeerIP("128.6.171.131");
        peer1.setPeerPort("24399");
        peer1.setPeerID("-AZ5500-oxeXC3F3IDPS");

        peer2.setPeerIP("128.6.171.130");
        peer2.setPeerPort("7310");
        peer2.setPeerID("-AZ5500-oxeXC3F3IDPD");

        peerList.add(peer1);
        peerList.add(peer2);

        return peerList;
    }

    /* Incomplete methods used for messing with the bitfield */
/*
    public static byte [] setBit(final byte [] bitfield, int piece_idx)
    {
        int byte_pos = piece_idx % 8;
        int byte_idx = piece_idx / 8;
        bitfield[byte_idx] |= (1 << byte_pos);
        return bitfield;
    }

    public static byte [] unsetBit(final byte[] bitfield, int piece_idx)
    {
        int byte_pos = piece_idx % 8;
        int byte_idx = piece_idx / 8;
        bitfield[byte_idx] &= ~(1 << byte_pos);
        return bitfield;
    }

    public static byte [] calcBitfield(TorrentInfo metadata, RandomAccessFile file) throws IOException
    {
        int piece_count = metadata.piece_hashes.length;
        int piece_length = metadata.piece_length;
        int file_length = metadata.file_length;
        int last_piece_length = file_length % piece_length;
        int byte_count = (int) Math.ceil(piece_count / 8.0);
        byte [] bitfield = new byte[byte_count];

       for (int i = 0; i < piece_count; i++)
       {
            byte [] file_segment = new byte[piece_length];

            if (i == piece_count - 1)
            {
                file_segment = new byte[last_piece_length];
            }

            file.read(file_segment);

            if (hashMatches(metadata.piece_hashes[i], file_segment))
            {
                bitfield = setBit(bitfield, i);
            }
            else
            {
                bitfield = unsetBit(bitfield, i);
            }
        }

        return bitfield;
    }

    public boolean isSet(byte[] arr, int bit)
    {
        int index = bit / 8;
        int bitPos = bit % 8;

        return (arr[index] >> bitPos & 1) == 1;
    }

    public boolean isUnset(byte[] arr, int bit)
    {
        return !(isSet(arr,bit));
    }
*/
}