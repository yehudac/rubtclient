/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

/* Define package structure and import required classes from this project */
package client.controllers;
import client.bencoder.*;
import client.peer.PeerListObject;
import client.utilities.Utils;
import client.utilities.ToolKit;

/* Import other useful classes */
import java.nio.ByteBuffer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.nio.charset.Charset;

/* Used for managing communications with the tracker */
public class TrackerController
{
    /* Useful keys for reading tracker response */
    private static final ByteBuffer peersKey = ByteBuffer.wrap(new byte[]{'p','e','e','r','s'});
    private static final ByteBuffer idKey = ByteBuffer.wrap(new byte[]{'p','e','e','r',' ','i','d'});
    private static final ByteBuffer ipKey = ByteBuffer.wrap(new byte[]{'i','p'});
    private static final ByteBuffer portKey = ByteBuffer.wrap(new byte[]{'p','o','r','t'}); 
    private static final ByteBuffer intKey = ByteBuffer.wrap(new byte[]{'i','n','t','e','r','v','a','l'});
    private static final ByteBuffer minIntKey = ByteBuffer.wrap(new byte[]{'m','i','n',' ','i','n','t','e','r','v','a','l'});

    private static DataController dataControl;

    private static ArrayList<PeerListObject> peerList; //Peer list from tracker
    private static int trackerInterval = 60000; //Interval with which to contact tracker

    public TrackerController(DataController dc)
    {
        dataControl = dc;
    }

    public ArrayList<PeerListObject> getPeerList()
    {
        return peerList;
    }

    public int getInterval()
    {
        return trackerInterval;
    }

    /* Update interval using tracker response. Set to minInterval if present, otherwise 1/2 * interval. If neither present, default to 60000 */
    private void updateInterval(Map<ByteBuffer, Object> responseData)
    {
        if(responseData.containsKey(minIntKey))
        {
            trackerInterval = (Integer) responseData.get(minIntKey) * 1000;
        }
        else if(responseData.containsKey(intKey))
        {
            trackerInterval = (int) (0.5 * (Double) responseData.get(intKey) * (double) 1000);
        }
    }

    /* Get Peers from tracker resposne. Looks specifically for 128.6.171.13x */
    private static ArrayList<PeerListObject> getRelevantPeers(Map<ByteBuffer, Object> responseData)
    {
        ArrayList<Object> peers = (ArrayList<Object>)responseData.get(peersKey); 
        ArrayList<PeerListObject> relevantPeers = new ArrayList<PeerListObject>();

        for (Object peer : peers)
        {
            Map<ByteBuffer,Object> client = (Map<ByteBuffer,Object>) peer;
            ByteBuffer id = (ByteBuffer) client.get(idKey);
            ByteBuffer ip = (ByteBuffer) client.get(ipKey);
            Integer port = (Integer) client.get(portKey);
            String sId = new String(id.array(), Charset.forName("UTF-8"));
            String sIP = new String(ip.array(), Charset.forName("UTF-8"));

            if (sIP.startsWith("128.6.171.13"))
            {
                PeerListObject newPeer = new PeerListObject();              
                String sPort = port.toString();

                newPeer.setPeerIP(sIP);
                newPeer.setPeerPort(sPort);
                newPeer.setPeerID(sId);

                relevantPeers.add(newPeer);
            }
        }

        return relevantPeers;
    }

    /* Updates the peer list. If it fails to do so, it loads a pre-populted list specific for this assignment */
    public void updatePeerList(String event)
    {
        byte[] trackerResponse = contactTracker(event);
        
        try
        {
            Map<ByteBuffer, Object> decodedTrackerResponse = (Map<ByteBuffer, Object>) Bencoder2.decode(trackerResponse);
            peerList = getRelevantPeers(decodedTrackerResponse);
        }
        catch(Exception e)
        {
            //Error getting peers
            System.out.println("Non-Fatal Error: Failed to pull peers from tracker. This is usually due to a failed decoding of the response. Reverting to generated list.");
            peerList = Utils.genFakePeerList();
        }
    }

    /* Using the scraped announce URL, this contacts the tracker with the appropriate parameters to notify it of a download start / finish */
    public byte[] contactTracker(String event)
    {
        String params = generateRequest(event);
        URL trackerURL = null;
        int responseCode = -1;
        HttpURLConnection con = null;

        try
        {
            trackerURL = new URL(dataControl.getMetadata().announce_url.toString() + params);	
        }
        catch(MalformedURLException mue)
        {
            System.out.println("ERROR: MalformedURLException");
            System.exit(1);
        }

        try
        {
            /* Establish connection with tracker */
            con = (HttpURLConnection) trackerURL.openConnection();
            con.setRequestMethod("GET");

            responseCode = con.getResponseCode();
        }
        catch(Exception e)
        {
            //Failed to contact tracker
            System.out.println("Non-Fatal Error: Could not contact tracker.");
        }

        if(responseCode != 200)
        {
            System.out.println("Non-Fatal Error: Tracker did not return 200 OK status code.");
        }

        /* Read tracker response */
        try
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            int character;
            StringBuffer response = new StringBuffer();

            while((character = in.read()) != -1)
            {
                response.append((char) character);
            }

            in.close();

            updateInterval((Map<ByteBuffer, Object>) Bencoder2.decode(response.toString().getBytes()));

            return response.toString().getBytes();
        }
        catch(Exception e)
        {
            System.out.println("Non-Fatal Error: Failed to read tracker response. Possibly bencoding fault.");
        }        

        return null;
    }

    /* Build the GET request for the tracker */
    public String generateRequest(String event)
    {
        StringBuilder get_request = new StringBuilder("?");

        get_request.append("info_hash=");
        get_request.append(Utils.bytesToHex(dataControl.getMetadata().info_hash.array()));
        get_request.append("&peer_id=");
        get_request.append(dataControl.getPeerID());
        get_request.append("&port=");
        get_request.append(dataControl.getPort());
        get_request.append("&uploaded=");
        get_request.append(dataControl.getFileController().getUploadedAmount());
        get_request.append("&downloaded=");
        get_request.append(dataControl.getFileController().getDownloadedAmount());
        get_request.append("&left=");
        get_request.append(dataControl.getMetadata().file_length - dataControl.getFileController().getDownloadedAmount());

        if(!event.equals("update"))
        {
            get_request.append("&event=");
            get_request.append(event);
        }

        return get_request.toString();
    }
}