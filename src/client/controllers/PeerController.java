/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

/* The Peer Controller class manages the download / upload peers through multithreading */

/* Define package structure and import required classes from this project */
package client.controllers;
import client.peer.*;
import client.threads.*;
import client.utilities.Utils;

/* Import other useful classes */
import java.util.ArrayList;

public class PeerController
{
	private static DataController dataControl;

	/* Track whether we are actively monitoring the incoming socket (uploads), the tracker thread (tracker updates), or our peers */
	private boolean socketActive = false;
	private boolean trackerActive = false;
	private boolean peersActive = false;

	/* Class that contains the code for the various threads */
	private pThreads threadController;

	/* Lists to track all peers (from tracker) as well peers we are connected to for downloading / uploading */
	private ArrayList<PeerListObject> masterPeerList;
	private ArrayList<Peer> currentDLPeers;
	private ArrayList<Peer> currentULPeers;

	/* Constructor for set up */
	public PeerController(DataController dc)
	{
		dataControl = dc;

		threadController = new pThreads(dataControl);

		currentDLPeers = new ArrayList<Peer>(2);
		currentULPeers = new ArrayList<Peer>(2);
	}

	/* Mutator/Accessor methods for looking at private data */
	public ArrayList<PeerListObject> getMasterList()
	{
		return masterPeerList;
	}

	public void setMasterList(ArrayList<PeerListObject> ml)
	{
		masterPeerList = ml;
	}

	public ArrayList<Peer> getDLList()
	{
		return currentDLPeers;
	}

	public ArrayList<Peer> getULList()
	{
		return currentULPeers;
	}

	public boolean getActiveStatus(int type)
	{
		switch(type)
		{
			case 0:
				return socketActive;
			case 1:
				return trackerActive;
			case 2:
				return peersActive;
			default:
				return false;
		}
	}

	/* Runs the peer controller. Sets all monitors to active and starts the threads */
	public void startController()
	{
		socketActive = true;
		pThreads.getSocketThread().start();

		trackerActive = true;
		pThreads.getTrackerThread().start();

		peersActive = true;
		pThreads.getPeerThread().start();
	}

	/* Stops the peer controller. Sets all monitors to inactive, stops downloading/uploading peers, and tries to rejoin all threads */
	public void stopController()
	{
		socketActive = false;
		trackerActive = false;
		peersActive = false;

		try
		{
			for(Peer singlePeer : currentDLPeers)
			{
				singlePeer.stopPeer();
			}

			for(Peer singlePeer : currentULPeers)
			{
				singlePeer.stopPeer();
			}

			pThreads.getSocketThread().join(3000);
			pThreads.getTrackerThread().join(10000);
			pThreads.getPeerThread().join(10000);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}

		/* Alert tracker we stopped */
		dataControl.getTrackerController().contactTracker("stopped");
	}

	/* Choose a peer from our master list that we are not already connected to */
	public PeerListObject chooseNextPeer()
	{
		boolean alreadyConnected = false;

		ArrayList<PeerListObject> peerPool = new ArrayList<PeerListObject>();

		for(PeerListObject peer : masterPeerList)
		{
			for(Peer peer2 : currentDLPeers)
			{
				/*System.out.println("DEBUG: peer1 ID: " + peer.getPeerID());
				System.out.println("DEBUG: peer2 ID: " + new String(peer2.getPeerID()));*/
				if(peer.getPeerID().equals(new String(peer2.getPeerID())))
				{
					alreadyConnected = true;
				}
			}

			if(!alreadyConnected)
			{
				peerPool.add(peer);
			}

			alreadyConnected = false;
		}
/*
		for(int i = 0; i < peerPool.size(); i++)
		{
			System.out.println("DEBUG: peerPool contains [ID: " + new String(peerPool.get(i).getPeerID()) + " / IP: " + peerPool.get(i).getPeerIP() + ":" + peerPool.get(i).getPeerPort() + "]");
		}
*/
		/* Choose first available peer */
		if(!peerPool.isEmpty())
		{
			return peerPool.get(0);
		}

		return null;
	}
}