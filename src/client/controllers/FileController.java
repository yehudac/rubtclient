/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

/* FileController is used to integrate different pieces from different peers into a single file */
   
/* Define package structure and import required classes from this project */
package client.controllers;

/* Import other useful classes */
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.ArrayList;

public class FileController
{
    /* Defines a piece structure to help track each piece */
	private class Piece
    {
        private int index; //Piece index
        private byte[] data; //Data contained within a piece
        private int havePiece; //Current status of the piece: 0 = don't have, 1 = have

        /* Constructor requires you to initialize the index in the beginning */
        public Piece(int index)
        {
            this.index = index;
        }

        /* Mutator methods for setting the private variables */
        public void updateStatus(int status)
        {
            this.havePiece = status;
        }

        public void updateData(byte[] data)
        {
            this.data = data;
        }

        /* Accessor methods for reading the private variables */
        public int getIndex()
        {
            return this.index;
        }

        public byte[] getData()
        {
            return this.data;
        }

        public int getStatus()
        {
            return this.havePiece;
        }
    }

    private static DataController dataControl; //Data controller contains useful objects for this class

    private String fileName; //Name of the file
    private File localFile; //File on the harddisk
    private int fileSize; //Size of the file

    private ArrayList<Piece> torrentPieces; //List of all pieces within this torrent

    private int bytesDownloaded = 0; //Amount of data currently downloaded
    private int bytesUploaded = 0; //Amount of data currently uploaded

    /* Constructor requires you to specify a filename for the local file and a data controller */
	public FileController(String fn, DataController dc)
	{
        try
        {
            dataControl = dc;

            fileName = fn;
            localFile = new File(fileName);
            fileSize = dataControl.getMetadata().file_length;

            /* Initialize a new array of pieces based on the number of pieces specified in the metadata */
            torrentPieces = new ArrayList<Piece>(dataControl.getTNumPieces());

            /* Create a new piece and add it to the list. Update its status to not downloaded */
            for(int i = 0; i < dataControl.getTNumPieces(); i++)
            {
                torrentPieces.add(new Piece(i));
                torrentPieces.get(i).updateStatus(0);
            }

            /* Check if we've already started downloading this file and just delete it if we have */
            if(localFile.exists())
            {
                localFile.delete();
            }

            /* Create File */
            localFile.createNewFile();
        }
        catch(Exception e)
        {
            //Failed to create file
            System.out.println("Fatal Error: Failed to create the local file on the hard disk.");
            System.exit(1);
        }
	}

    /* Get the next piece to download, ignores rarity */
    public int getNextPieceToDL()
    {
        /* Return index of first piece we don't have */
        for(int i = 0; i < torrentPieces.size(); i++)
        {
            if(torrentPieces.get(i).getStatus() == 0)
            {
                return torrentPieces.get(i).getIndex();
            }
        }

        /* Return -1 if we don't have any more pieces to download */
        return -1;
    }

    /* Mutator to update status of a piece */
    public void updatePiece(int index, int status)
    {
        torrentPieces.get(index).updateStatus(status);
    }

    /* Add a recently downloaded piece into our collection */
    public void addPieceToCollection(int index, byte[] data)
    {
        Piece targetPiece = torrentPieces.get(index);

        if(targetPiece.getStatus() == 1)
        {
            return;
        }

        targetPiece.updateData(data);
        targetPiece.updateStatus(1);

        bytesDownloaded += data.length;

        /* If we have all the pieces, save the file and alert the tracker we finished */
        if(haveAllPieces())
        {
            saveFile();
            dataControl.getTrackerController().contactTracker("completed");
        }
    }

    /* Method to check if we've downloaded all the pieces */
    public boolean haveAllPieces()
    {
        for(int i = 0; i < torrentPieces.size(); i++)
        {
            if(torrentPieces.get(i).getStatus() != 1)
            {
                return false;
            }
        }

        return true;
    }

    /* Return data for a specified piece to upload */
    public byte[] getUploadPiece(int index)
    {
        if(torrentPieces.get(index).getStatus() == 1)
        {
            bytesUploaded += torrentPieces.get(index).getData().length;
            return torrentPieces.get(index).getData();
        }

        return null;
    }

    /* Save a part file if we close before finishing the download */
    private void saveIncompleteFile()
    {        
        try
        {
            File partLocalFile = new File(fileName + ".part");
            FileOutputStream fos = new FileOutputStream(partLocalFile);

            if(partLocalFile.exists())
            {
                partLocalFile.delete();
            }

            partLocalFile.createNewFile();

            for(int i = 0; i < torrentPieces.size(); i++)
            {
                if(torrentPieces.get(i).getStatus() == 1)
                {
                    fos.write(torrentPieces.get(i).getData());
                }
            }

            fos.flush();
            fos.close();

            System.out.println("Info: Download incomplete upon exit. Saved PART file.");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("Fatal Error: Failed to save the temp file to the local hard disk.");
            System.exit(1);
        }
    }

    /* Write the file to disk. Will save PART file if download is not complete */
    public void saveFile()
    {
        if(!haveAllPieces())
        {
            saveIncompleteFile();
            return;
        }

        try
        {
            FileOutputStream fos = new FileOutputStream(localFile);

            for(int i = 0; i < torrentPieces.size(); i++)
            {
                fos.write(torrentPieces.get(i).getData());
            }

            fos.flush();
            fos.close();

            System.out.println("Info: Download Complete");
            System.exit(0);

            File partLocalFile = new File(fileName + ".part");
            if(partLocalFile.exists())
            {
                partLocalFile.delete();
            }
        }
        catch(Exception e)
        {
            System.out.println("Fatal Error: Failed to save the file to the local hard disk.");
            System.exit(1);
        }
    }

    /* Self-explanatory accessor methods. Used for tracker updates */
    public int getDownloadedAmount()
    {
        return bytesDownloaded;
    }

    public int getUploadedAmount()
    {
        return bytesUploaded;
    }

    /* Method for selecting piece based on rarity. Unfinished */
    /* Returns least popular piece id owned by target */ 
    /*private int nextPieceId(Peer target)
    {
        Map<Integer, Integer> popularities = new Hashmap<Integer,Integer>(); 

        for (int i = 0; i < metadata.piece_hashes.length; i++)
        {
            popularities.put(i,0);
        }

        for (Peer p : dataControl.getPeerController().getMasterList())
        {
            if (peer.bitfield != null)
            {
                for (int i = 0; i < peer.bitfield.length; i++)
                {
                    if (Utils.isSet(peer.bitfield, i))
                    { 
                        popularities.put(i, popularities.get(i) + 1);
                    }
                }
            }
        }
        
        int toRequestId = 0; 

        for (int i = 0; i < this.bitfield.length; i++)
        {
            if (Utils.isUnset(this.bitfield, i))
            {
                if (popularities.get(toRequestId) > popularities.get(i))
                { 
                    toRequestId = i; 
                } 
            } 
        } 

        return toRequestId; 
    }*/
}