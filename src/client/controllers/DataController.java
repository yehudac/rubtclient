/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

/* The DataController class houses objects that are useful to multiple classes. This alleviates the issue of having to give each class a copy of these objects */

/* Define package structure and import required classes from this project */
package client.controllers;
import client.utilities.TorrentInfo;

/* Import other useful classes */
import java.net.ServerSocket;

public class DataController
{
    private static TorrentInfo torrentMetadata;
    private static FileController fileControl;
    private static TrackerController trackerControl;
    private static PeerController peerControl;

    private static byte[] peerID;

    /* ServerSocket and the port it listens on for incoming peer connections */
    private static ServerSocket listener;
    private static int listeningPort;

    /* Useful torrent information */
    private static int tNumberPieces; //Total number of pieces in the torrent file
    private static int tDefaultPieceLength; //The default piece length of all pieces except for the last one
    private static int tLastPieceLength; //The piece length of the last piece specifically

    /* Empty constructor */
    public DataController()
    {

    }

    /* Mutator methods for setting all of the contained variables */
    public void setMetadata(TorrentInfo md)
    {
        torrentMetadata = md;
        updateTorrentVars();
    }

    public void setFileController(FileController fc)
    {
        fileControl = fc;
    }

    public void setTrackerController(TrackerController tc)
    {
        trackerControl = tc;
    }

    public void setPeerController(PeerController pc)
    {
        peerControl = pc;
    }

    public void setPeerID(byte[] pid)
    {
        peerID = pid;
    }

    public void setListener(ServerSocket sock)
    {
        listener = sock;
    }

    public void setListeningPort(int port)
    {
        listeningPort = port;
    }

    /* Mutator methods for accessing the private variables in this controller */
    public TorrentInfo getMetadata()
    {
        return torrentMetadata;
    }

    public FileController getFileController()
    {
        return fileControl;
    }

    public TrackerController getTrackerController()
    {
        return trackerControl;
    }

    public PeerController getPeerController()
    {
        return peerControl;
    }

    public byte[] getPeerID()
    {
        return peerID;
    }

    public ServerSocket getListener()
    {
        return listener;
    }

    public int getPort()
    {
        return listeningPort;
    }

    public int getTNumPieces()
    {
        return tNumberPieces;
    }

    public int getTDefPieceLen()
    {
        return tDefaultPieceLength;
    }

    public int getTLastPieceLen()
    {
        return tLastPieceLength;
    }

    /* Updates the useful torrent info variables from the metadata */
    private void updateTorrentVars()
    {
        tNumberPieces = torrentMetadata.piece_hashes.length;
        tDefaultPieceLength = torrentMetadata.piece_length;
        tLastPieceLength = torrentMetadata.file_length - ((tNumberPieces - 1) * tDefaultPieceLength);
    }
}