/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

/* Responsible for managing downloads / uploads */

/* Define package structure and import required classes from this project */
package client.peer;
import client.controllers.*;
import client.message.*;
import client.utilities.*;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.nio.ByteBuffer;

import java.util.Arrays;

public class Peer
{	
	private static DataController dataControl;

    /* Socket info for communicating with the other guy */
    private String peerIP;
    private int peerPort;
    private Socket peerSocket = null;
    private byte[] remotePeerID;

    /* Streams for reading / writing messages */
    private DataInputStream dins = null;
    private DataOutputStream douts = null;

    /* Tracking vars */
    private boolean remoteChoking = true;
    private boolean remoteInterested = false;

    private boolean localChoking = true;
    private boolean localInterested = false;

    private int currentReqIndex = -1;

    private Thread downloadThread;
    private Thread socketThread;
    private boolean weActive = false;

    public Peer(DataController dc, PeerListObject remoteHost)
    {
        dataControl = dc;
        peerIP = remoteHost.getPeerIP();
        peerPort = remoteHost.getPeerPort();
        remotePeerID = remoteHost.getPeerID().getBytes();

        try
        {
	        peerSocket = new Socket(peerIP, peerPort);
	        dins = new DataInputStream(peerSocket.getInputStream());
	        douts = new DataOutputStream(peerSocket.getOutputStream());
            handshakePeer();
	    }
	    catch(Exception e)
	    {
	    	//ERROR
	    }
    }

    public Peer(DataController dc, Socket socket)
    {
    	dataControl = dc;

    	peerSocket = socket;

    	try
    	{
    		dins = new DataInputStream(peerSocket.getInputStream());
    		douts = new DataOutputStream(peerSocket.getOutputStream());
    		peerSocket.setSoTimeout(20000);

    		byte[] handshake = new byte[68];
    		dins.readFully(handshake);

    		System.arraycopy(handshake, 0, remotePeerID, 0, 20);
    		peerIP = peerSocket.getInetAddress().getHostAddress();
    		peerPort = peerSocket.getPort();
    	}
    	catch(Exception e)
    	{
    		//ERROR
    	}
    }

    /* Run peer */
    public void startPeer()
    {
    	weActive = true;
    	(socketThread = new Thread(new Runnable()
    	{
    		public void run()
    		{
                Message toPeer;
                Message fromPeer = null;
                boolean msgRead = false;

    			toPeer = new Message(1, Message.TYPE_INTERESTED);
    			MessageUtils.writeMessage(douts, toPeer);

                /* Handle messages */
    			while(!peerSocket.isClosed() && weActive)
    			{   
    				try
    				{ 
                        fromPeer = MessageUtils.readMessage(dins);

						if(fromPeer.getMessageType() == Message.TYPE_CHOKE) /* Let them know we're interested */
						{
                            Utils.peerDebugMessage("CHOKE", remotePeerID, peerIP, peerPort);

							remoteChoking = true;
							toPeer = new Message(1, Message.TYPE_INTERESTED);
							MessageUtils.writeMessage(douts, toPeer);
						}
						else if(fromPeer.getMessageType() == Message.TYPE_UNCHOKE) /* Ask for a piece */
						{
                            Utils.peerDebugMessage("UNCHOKE", remotePeerID, peerIP, peerPort);
							remoteChoking = false;
                            requestPiece();
						}
						else if(fromPeer.getMessageType() == Message.TYPE_INTERESTED) /* Unchoke randomly */
						{
                            Utils.peerDebugMessage("INTERESTED", remotePeerID, peerIP, peerPort);

							if(Math.random() > 0.5)
							{
								localChoking = false;
								remoteInterested = true;
								toPeer = new Message(1, Message.TYPE_UNCHOKE);
								MessageUtils.writeMessage(douts, toPeer);
							}
						}
						else if(fromPeer.getMessageType() == Message.TYPE_UNINTERESTED) /* Update vars */
						{
                            Utils.peerDebugMessage("UNINTERESTED", remotePeerID, peerIP, peerPort);

							remoteInterested = false;
						}
						else if(fromPeer.getMessageType() == Message.TYPE_HAVE) /* Ignore */
						{
                            Utils.peerDebugMessage("HAVE", remotePeerID, peerIP, peerPort);
						}
						else if(fromPeer.getMessageType() == Message.TYPE_BITFIELD) /* Ignore */
						{
                            Utils.peerDebugMessage("BITFIELD", remotePeerID, peerIP, peerPort);
						}
						else if(fromPeer.getMessageType() == Message.TYPE_REQUEST) /* Upload piece if not choking */
						{
                            Utils.peerDebugMessage("REQUEST", remotePeerID, peerIP, peerPort);

							if(!localChoking)
							{
                                byte[] data = dataControl.getFileController().getUploadPiece(fromPeer.getPayloadIndex());

                                if(data == null)
                                {
                                    data = new byte[0];
                                }

                                toPeer = new Message(9 + fromPeer.getPayloadReqLen(), Message.TYPE_PIECE, fromPeer.getPayloadIndex(), fromPeer.getPayloadBegin(), data);
                                MessageUtils.writeMessage(douts, toPeer);
							}
						}
						else if(fromPeer.getMessageType() == Message.TYPE_PIECE) /* Save piece */
						{
                            Utils.peerDebugMessage("PIECE", remotePeerID, peerIP, peerPort);

							//Handle piece

                            if(!(verifyPieceHash(fromPeer.getPayloadIndex(), fromPeer.getPayloadBlock())))
                            {
                                System.out.println("WARNING: Piece hashes do not match for piece: " + fromPeer.getPayloadIndex() + ". Ignoring.");
                            }
                            else
                            {
                                int pieceLen = determinePieceLength();
                                byte[] pieceData = new byte[pieceLen];

                                System.arraycopy(fromPeer.getPayloadBlock(), 0, pieceData, 0, pieceLen);

                                dataControl.getFileController().addPieceToCollection(fromPeer.getPayloadIndex(), fromPeer.getPayloadBlock());

                                toPeer = new Message(5, Message.TYPE_HAVE, fromPeer.getPayloadIndex());
                                MessageUtils.writeMessage(douts, toPeer);
                            }

							//Request next piece
                            requestPiece();
						}
					}
					catch(Exception e)
					{
						System.out.println("Error: Failed to handle incoming message");
					}
    			}
    		}
    	})).start();
    }

    /* Safely close peer */
    public void stopPeer()
    {
    	weActive = false;

    	Message toPeer = new Message(1, Message.TYPE_CHOKE);
    	MessageUtils.writeMessage(douts, toPeer);

    	try
    	{
    		socketThread.join(8000);
    	}
    	catch(Exception e)
    	{
    		System.out.println("Non-Fatal Error: Failed to rejoin socket");
    	}

    	disconnect();

    	dataControl.getFileController().updatePiece(currentReqIndex, 0);
    }

    /* Get next download piece from other guy */
    private void requestPiece()
    {
    	Message toPeer;

    	if(remoteChoking)
    	{
    		toPeer = new Message(1, Message.TYPE_INTERESTED);
    		MessageUtils.writeMessage(douts, toPeer);
    	}
    	else
    	{
            currentReqIndex = dataControl.getFileController().getNextPieceToDL();

    		if(currentReqIndex != -1)
    		{
    			int pieceLen = determinePieceLength();

    			toPeer = new Message(13, Message.TYPE_REQUEST, currentReqIndex, 0, pieceLen);
    			MessageUtils.writeMessage(douts, toPeer);
    		}
    		else
    		{
    			//No more pieces to request
                System.out.println("Have all pieces");
                System.exit(0);
    		}
    	}
    }

    public byte[] getPeerID()
    {
        return remotePeerID;
    }

    public String getPeerIP()
    {
        return peerIP;
    }

    public int getPeerPort()
    {
        return peerPort;
    }

    public void setRemotePID(byte[] remotePID)
    {
        remotePeerID =  remotePID;
    }

    /* Create the handshake message */
    public byte[] createHandshake()
    {
        byte[] infohash = dataControl.getMetadata().info_hash.array();
        byte[] handshake = new byte[68];
        byte[] handshake2 = "BitTorrent protocol".getBytes();
        handshake[0] = 19;
        System.arraycopy(handshake2, 0, handshake, 1, handshake2.length);
        System.arraycopy(infohash, 0, handshake, 9 + handshake2.length, infohash.length);
        System.arraycopy(dataControl.getPeerID(), 0, handshake, 9 + handshake2.length + infohash.length, dataControl.getPeerID().length);

        return handshake;
    }

    /* Perform the handshake with the peer and verify the information he kicks back */
    public void handshakePeer() throws IOException
    {
        byte[] handshakeMessage = createHandshake();
        byte[] handshakeResponse = new byte[68];
        byte[] localInfoHash = dataControl.getMetadata().info_hash.array();

        try
        {
	        douts.write(handshakeMessage);
	        douts.flush();

	        dins.readFully(handshakeResponse);
	    }
	    catch(IOException ioe)
	    {
            System.out.println("Error: Unable to handshake");
	    }
/*
        for(int i = 28; i < 48; i++)
        {
            if(localInfoHash[i - 28] != handshakeResponse[i])
            {
                System.out.println("Peer info_hash does not match metadata info_hash!");
                System.exit(1);
            }
        }*/
/*
        for(int i = 48; i < 68; i++)
        {
            if(this.remotePeerID[i - 48] != handshakeResponse[i])
            {
                System.out.println("Reported remote peer ID does not match expected remote peer ID!");
                System.exit(1);
            }
        }*/
    }

    /* Verify the hash of any piece we recieve against the metadata */
    public boolean verifyPieceHash(int offset, byte[] piece)
    {
        byte[] calculatedHash = new byte[20];

        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1");

            calculatedHash = md.digest(piece);
            return (ByteBuffer.wrap(calculatedHash).equals(dataControl.getMetadata().piece_hashes[offset]));
        }
        catch (NoSuchAlgorithmException nsae)
        {
            System.err.println("ERROR: Couldn't find SHA-1 algorithm to compute piece hash!");
            nsae.printStackTrace();
            return false;
        }

    }

    private int determinePieceLength()
    {
        int actualPieceLen = dataControl.getTDefPieceLen();

    	if(currentReqIndex == dataControl.getTNumPieces() - 1)
    	{
    		actualPieceLen = dataControl.getTLastPieceLen();
    	}

    	return actualPieceLen;
    }

    /* Safely close the connections */
    public void disconnect()
    {
        try
        {
            dins.close();
            douts.close();
            peerSocket.close();
        }
        catch(IOException ioe)
        {
            System.err.println("ERROR: Failed to close the input/output streams and associated socket!");
            ioe.printStackTrace();
        }        
    }
}