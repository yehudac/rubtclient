/*  ************************
	Yehuda Cohen
	Luciano Taranto
	Internet Technology
	Section 02
	Phase 3
	************************
*/

package client.peer;

/* Class that manages the peer list objects. Each object consists of an IP, port, and peer ID */
public class PeerListObject 
{
	private String ipAddress;
	private int port;
	private String id;

	/* Empty constructor */
	public PeerListObject()
	{	
	}

	/* Mutator methods for modifying private data */
	public void setPeerIP(String ip)
	{
		ipAddress = ip;
	}

	public void setPeerPort(String port)
	{
		this.port = Integer.parseInt(port);
	}

	public void setPeerID(String id)
	{
		this.id = id;
	}

	/* Accessor Methods for looking at private data */
	public String getPeerIP()
	{
		return this.ipAddress;
	}

	public int getPeerPort()
	{
		return this.port;
	}

	public String getPeerID()
	{
		return this.id;
	}
}