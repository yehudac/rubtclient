/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

package client.message;
import client.controllers.*;

public class Message
{
	/* Message ID Fields */
	public static byte TYPE_KEEPALIVE = -1;
	public static byte TYPE_CHOKE = 0;
	public static byte TYPE_UNCHOKE = 1;
	public static byte TYPE_INTERESTED = 2;
	public static byte TYPE_UNINTERESTED = 3;
	public static byte TYPE_HAVE = 4;
	public static byte TYPE_BITFIELD = 5;
	public static byte TYPE_REQUEST = 6;
	public static byte TYPE_PIECE = 7;
	
	/* Length prefix and message type */
	private int length;
	private byte id;

	/* Payload Variables - if needed */
	private int index; /* Used for have, request, piece */
	private int begin; /* Used for request, piece */
	private int requestedLength;
	private byte[] bitfield; /* Used for bitfield */
	private byte[] block; /* Used for piece */

	/* Defines a message with no payload - choke/unchoke/interested/uninterested */
	public Message(int len, byte type)
	{
		this.length = len;
		this.id = type;
	} 

	/* Defines a HAVE message */
	public Message(int len, byte type, int index)
	{
		this.length = len;
		this.id = type;
		this.index = index;
	}

	/* Defines a BITFIELD message */
	public Message(int len, byte type, byte[] bf)
	{
		this.length = len;
		this.id = type;
		this.bitfield = bf;
	}

	/* Defines a REQUEST message */
	public Message(int len, byte type, int index, int begin, int req_len)
	{
		this.length = len;
		this.id = type;
		this.index = index;
		this.begin = begin;
		this.requestedLength = req_len;
	}

	/* Defines a PIECE message */
	public Message(int len, byte type, int index, int begin, byte[] block)
	{
		this.length = len;
		this.id = type;
		this.index = index;
		this.begin = begin;
		this.block = block;
	}

	/* Accessor Methods */
	public int getMessageLength()
	{
		return this.length;
	}

	public byte getMessageType()
	{
		return this.id;
	}

	public int getPayloadIndex()
	{
		return this.index;
	}

	public int getPayloadBegin()
	{
		return this.begin;
	}

	public int getPayloadReqLen()
	{
		return this.requestedLength;
	}

	public byte[] getPayloadBitfield()
	{
		return this.bitfield;
	}

	public byte[] getPayloadBlock()
	{
		return this.block;
	}
}