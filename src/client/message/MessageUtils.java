/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

package client.message;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/* Class for reading and sending peer messages */
public class MessageUtils
{
	public static Message readMessage(DataInputStream din)
	{
		try
		{
			Message msg = null;

			/* Read in the length first */
			int len = din.readInt();

			/* If length is 0, this must be a KEEP ALIVE. Go no further */
			if(len == 0)
			{
				msg = new Message(0, Message.TYPE_KEEPALIVE);
				return msg;
			}

			/* Length was not 0, get the message type */
			byte type = din.readByte();

			/* Check the basic messages that have no payload */
			if(len == 1 && (type == Message.TYPE_CHOKE || type == Message.TYPE_UNCHOKE || type == Message.TYPE_INTERESTED || type == Message.TYPE_UNINTERESTED))
			{
				msg = new Message(len, type);
			}
			else if(len == 5 && type == Message.TYPE_HAVE) /* See if its a HAVE. If it is, read the pieceIndex */
			{
				int pieceIndex = din.readInt();
				msg = new Message(len, type, pieceIndex);
			}
			else if(len == 13 && type == Message.TYPE_REQUEST) /* See if its a REQUEST. If it is, read the index, offset, length */
			{
				int pieceIndex = din.readInt();
				int pieceOffset = din.readInt();
				int pieceLength = din.readInt();

				msg = new Message(len, type, pieceIndex, pieceOffset, pieceLength);
			}
			else if(len >= 9 && type == Message.TYPE_PIECE) /* See if its a PIECE. If it is, read the index, offset, and the actual data */
			{
				int pieceIndex = din.readInt();
				int pieceOffset = din.readInt();
				int pieceLength = len - 9;

				byte[] piece = new byte[pieceLength];
				din.readFully(piece, 0, pieceLength);

				msg = new Message(len, type, pieceIndex, pieceOffset, piece);
			}
			else if(len >= 1 && type == Message.TYPE_BITFIELD) /* See if its a BITFIELD. If it is, read the bitfield data off the wire */
			{
				int bfLength = len - 1;

				byte[] bfield = new byte[bfLength];
				din.readFully(bfield, 0, bfLength);

				msg = new Message(len, type, bfield);
			}

			return msg;
		}
		catch(IOException ioe)
		{
			System.err.println("ERROR: IOException occurred while reading the message!");
			ioe.printStackTrace();
			System.exit(1);
		}

		return null;
	}

	public static void writeMessage(DataOutputStream dout, Message msg)
	{
		try
		{
			if(msg == null)
			{
				System.out.println("Attempted to write a NULL message!");
				System.exit(1);
			}

			/* Start by writing the length */
			dout.writeInt(msg.getMessageLength());

			/* Stop if the message is a KEEPALIVE */
			if(msg.getMessageLength() == 0 && msg.getMessageType() == Message.TYPE_KEEPALIVE)
			{
				return;
			}

			/* If not a KEEPALIVE, write the message type */
			byte messageType = msg.getMessageType();
			dout.writeByte(messageType);

			/* Write extra information if the message contains a payload */
			if(messageType == Message.TYPE_HAVE)
			{
				dout.writeInt(msg.getPayloadIndex());
			}
			else if(messageType == Message.TYPE_BITFIELD)
			{
				dout.write(msg.getPayloadBitfield());
			}
			else if(messageType == Message.TYPE_REQUEST)
			{
				dout.writeInt(msg.getPayloadIndex());
				dout.writeInt(msg.getPayloadBegin());
				dout.writeInt(msg.getPayloadReqLen());
			}
			else if(messageType == Message.TYPE_PIECE)
			{
				dout.writeInt(msg.getPayloadIndex());
				dout.writeInt(msg.getPayloadBegin());
				dout.write(msg.getPayloadBlock());
			}

			dout.flush();
		}
		catch(IOException ioe)
		{
			System.err.println("ERROR: IOException occurred while writing the message!");
			ioe.printStackTrace();
			System.exit(1);
		}
	}
}