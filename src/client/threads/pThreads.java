/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

/* This class assists with multithreading. Defines different peer threads. */

/* Define package structure and import required classes from this project */
package client.threads;
import client.peer.*;
import client.controllers.DataController;

/* Import other useful classes */
import java.net.Socket;

public class pThreads
{
	private static DataController dataControl;

	/* Use this to know if we should send event=started when speaking to tracker */
	private static boolean firstRun = true;

	public pThreads(DataController dc)
	{
		dataControl = dc;
	}

	public static Thread getTrackerThread()
	{
		return trackerThread;
	}

	public static Thread getSocketThread()
	{
		return socketThread;
	}

	public static Thread getPeerThread()
	{
		return peerThread;
	}

	/* Thread for incoming socket connections (uploads) */
	private static Thread socketThread = new Thread(new Runnable()
	{
		public void run()
		{
			try
			{
				dataControl.getListener().setSoTimeout(0);
			}
			catch (Exception e)
			{
				//Error
				System.out.println("Non-Fatal Error: Failed to set socket timeout.");
			}

			while(dataControl.getPeerController().getActiveStatus(0))
			{
				/* Attempt to accept new incoming connections for upload */
				try
				{
					Socket peerSocket = dataControl.getListener().accept();
					Peer newPeer = new Peer(dataControl, peerSocket);

					if(newPeer.getPeerID() != null)
					{
						if(dataControl.getPeerController().getULList().size() < 2)
						{
							dataControl.getPeerController().getULList().add(newPeer);
						}
					}
				}
				catch (Exception e)
				{
					if(!e.getMessage().contains("Socket closed"))
					{
						System.out.println("Fatal Error: Failed to accept incoming connection.");
						System.exit(1);
					}
				}
			}
		}
	});

	/* Thread for scraping tracker */
	private static Thread trackerThread = new Thread(new Runnable()
	{
		public void run()
		{
			while(dataControl.getPeerController().getActiveStatus(1))
			{
				//Tell tracker to get peers. Then get its list.
				if(firstRun)
				{
					dataControl.getTrackerController().updatePeerList("started");
					firstRun = false;
				}
				else
				{
					dataControl.getTrackerController().updatePeerList("update");
				}


				dataControl.getPeerController().setMasterList(dataControl.getTrackerController().getPeerList());

				if(dataControl.getPeerController().getMasterList() == null)
				{
					//Error - cant reach tracker
					System.out.println("Failed to contact tracker for updated peer list.");
					break;
				}

				if(dataControl.getPeerController().getMasterList().isEmpty())
				{
					//Error - no peers
					System.out.println("Tracker returned no peers.");
				}

				try
				{
					for(int i = 0; i < 10; i++)
					{
						Thread.sleep((int) (dataControl.getTrackerController().getInterval() / 10f));

						if(!dataControl.getPeerController().getActiveStatus(1))
						{
							break;
						}
					}
				}
				catch(InterruptedException e)
				{
					//Interrupted
				}
			}
		}			
	});
	
	/* Thread for adding new download peers */
	private static Thread peerThread = new Thread(new Runnable()
	{
		public void run()
		{
			while(dataControl.getPeerController().getActiveStatus(2))
			{
				try
				{
					Thread.sleep(3000);
				}
				catch(InterruptedException e)
				{
					//Interrupted
				}

				//Can check if we are at capacity and drop slowest download connection

				if(dataControl.getPeerController().getDLList().size() < 1 && dataControl.getPeerController().getDLList().size() < dataControl.getPeerController().getMasterList().size())
				{
					for(int i = 0; i < 1 && dataControl.getPeerController().getDLList().size() <= 1; i++)
					{
						PeerListObject peer = dataControl.getPeerController().chooseNextPeer();

						if(peer == null)
						{
							return;
						}

						Peer newPeerConnec = new Peer(dataControl, peer); // Set up new peer connection and run handshake

						if(newPeerConnec != null)
						{
							System.out.println("DEBUG: Added new peer [ID: " + new String(peer.getPeerID()) + " / IP: " + peer.getPeerIP() + ":" + peer.getPeerPort() + "]");
							newPeerConnec.startPeer();
							dataControl.getPeerController().getDLList().add(newPeerConnec);
						}
					}	
				}

				for(int i = 0; i < 9; i++)
				{
					try
					{
						Thread.sleep(3000);
					}
					catch(InterruptedException e)
					{
						//Interrupted
					}

					if(!dataControl.getPeerController().getActiveStatus(2))
					{
						break;
					}
				}
			}
		}
	});

}