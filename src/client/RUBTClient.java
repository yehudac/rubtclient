/* ************************
   Yehuda Cohen
   Luciano Taranto
   Internet Technology
   Section 02
   Phase 3
   ************************
*/

   //128.6.171.131:24399
   //128.6.171.130:7310

/* Define package structure and import required classes from this project */
package client;
import client.bencoder.*;
import client.controllers.*;
import client.message.*;
import client.peer.*;
import client.utilities.*;

/* Import other useful classes */
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.util.*;
import java.security.NoSuchAlgorithmException;
import java.lang.ClassCastException;
import java.nio.charset.CharacterCodingException;


public class RUBTClient
{
    /* TRUNCATED_RESPONSE stoers a safe and clean copy of the response from the GET request to the ANNOUNCE URL */
    static final String TRUNCATED_RESPONSE = "d8:completei2e10:downloadedi113e10:incompletei6e8:intervali120e12:min intervali120e5:peersld2:ip13:128.6.171.1317:peer id20:-AZ5400-RcPjQXoVG7wb4:porti24399eed2:ip13:128.6.171.1307:peer id20:-AZ5400-xYuwOWOl9kX84:porti38309eed2:ip13:68.196.224.257:peer id20:JordanIsTheBest111114:porti5555eed2:ip10:47.20.3.227:peer id20:wByE1ag2xmTGvXQFaojH4:porti6881eed2:ip12:100.1.85.2307:peer id11:[B@6030e2804:porti6889eed2:ip12:68.37.14.1007:peer id20:ianstest1111111111114:porti8080eed2:ip12:100.1.85.2307:peer id20:-AZ5400-Rj9RBEBjpasi4:porti33574eeee";
    static final String PEER_PREFIX = "-AZ54"; /* PeerID prefix to look for in the response from the GET request to the announce URL */

    private static DataController dataControl;

    /* Load the metadata from specified torrent file */
    public static void loadMetadata(String torrentFile)
    {
        try
        {
    	   Path torrentPath = Paths.get(torrentFile);
    	   byte[] torrentFileData = Files.readAllBytes(torrentPath);
    	   dataControl.setMetadata(new TorrentInfo(torrentFileData));
        }
        catch(Exception e)
        {
            System.out.println("Fatal Error: Failed to load metadata");
            System.exit(1);
        }
    }
	
    public static void main(String[] args) throws IOException, BencodingException, InterruptedException, NoSuchAlgorithmException
    {
        String torrentFileName;
        String destFile;
        byte [] fileData;
        Path path;

        /* Verify that we got 2 arguments, the torrent file name and save file name. If so, parse the arguments. If not, print error and quit. */
        if (args.length == 2)
        {
        	System.out.println("== RUBTClient. Press enter to exit ==");
        	dataControl = new DataController();

        	try
        	{		        
		        /* Read in the cmd-line args */
		        torrentFileName = args[0];
		        destFile = args[1];

		        /* Parse the torrent file for information */
		        loadMetadata(torrentFileName);

                /* Initialize controllers, get a peer ID, start listening for incoming connections, and start peer controller */
		        dataControl.setFileController(new FileController(destFile, dataControl));
		        dataControl.setTrackerController(new TrackerController(dataControl));
		        dataControl.setPeerID(Utils.generatePeerID());

		        dataControl.setListener(Utils.openListener());
		        dataControl.setListeningPort(dataControl.getListener().getLocalPort());

		        dataControl.setPeerController(new PeerController(dataControl));
		        dataControl.getPeerController().startController();

                /* Wait for user input */
		        System.in.read();

                /* Stop everything, alert tracker, and save file or partial file */
		        dataControl.getPeerController().stopController();

		       	dataControl.getTrackerController().contactTracker("stopped");

                dataControl.getFileController().saveFile();

                /* Close listening socket */
		        try
		        {
		        	dataControl.getListener().close();
		        }
		        catch(Exception e)
				{
					System.out.println("Non-Fatal Error: Failed to close socket.");
				}
			}
			catch(Exception e)
			{
				System.out.println("Fatal Error: Failed to initialize client.");
                System.exit(1);
			}
        }
  		else
    	{
            /* Print error and quit if not given exactly 2 arguments */
            System.out.println("Invalid number of arguments. Program requires two arguments, the first being the torrent file and the second being local file to save the download as.\n");
            return;
    	}
        
    }
}